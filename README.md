# Run inside a docker container
You can run this program inside a docker container.
After the program exits result will be saved in the same directory where you started the container.

Run the image with:
```
docker run -v $(pwd):/app -it suruaku/course-python:latest
```

To use the program give it a file name e.g. `MoodleData2016.csv` and a year to be analysed, in this case it is `2016`(because the given file does not have any other year's data).
Then analyse and save the result e.g. use options `2` & `3` to get per day analysis.

# Build it yourself
You can also build the image yourself. Run the following command in this directory.
```
docker build -t course-python .
```

# Disclaimer
The program does not handle every wrong input given by the user.
