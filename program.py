import HTLib


def paaohjelma():
    # Luodaan tarvittavat listat
    kaikkiData = []
    paivaData = []
    kuukausiData = []
    while True:
        valinta = HTLib.valikko()
        # Lue sähköntuotantotiedot
        if valinta == "1":
            tiedostonNimi = str(input("Anna luettavan tiedoston nimi: "))
            vuosi = str(input("Anna analysoitava vuosi: "))
            kaikkiData = HTLib.lue(tiedostonNimi, kaikkiData, vuosi)
        # Analysoi päivätuotanto
        elif valinta == "2":
            paivaData = HTLib.paivaAnalyysi(kaikkiData, paivaData)
        # Tallenna päivätuotanto
        elif valinta == "3":
            HTLib.paivaTallenna(paivaData, vuosi)
        # Analysoi kuukausituotanto
        elif valinta == "4":
            kuukausiData = HTLib.kuukausiAnalyysi(kaikkiData, kuukausiData)
        # Analysoi tuntituotanto
        elif valinta == "5":
            # Luodaan matriisi (i <3 matriisi) tuntituotannolle
            tuntiData = [[0 for x in range(24)] for y in range(13)]
            tuntiData = HTLib.tuntiAnalyysi(kaikkiData, tuntiData)
        # Tallenna kuukausituotanto
        elif valinta == "6":
            HTLib.kuukausiTallenna(kuukausiData, vuosi)
        # Tallenna tuntituotanto
        elif valinta == "7":
            HTLib.tuntiTallenna(tuntiData, vuosi)
        # Lopeta
        elif valinta == "0":
            print("Kiitos ohjelman käytöstä.")
            break
        # Koska ohjelmointi on hauskaa!
        elif valinta == "This is so sad, Alexa play Despacito.":
            HTLib.Alexa()
        # Koska me vertaillaan merkkejä eikä numeroita
        # yhdellä else lauseella voidaan hoitaa kaikki väärät syötteet
        else:
            print("Väärä syöte.")

if __name__ == "__main__":
    paaohjelma()
