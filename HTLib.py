import datetime
import sys
import webbrowser


def valikko():
    print("Anna haluamasi toiminnon numero seuraavasta valikosta:")
    print("1) Lue sähköntuotantotiedot")
    print("2) Analysoi päivätuotanto")
    print("3) Tallenna päivätuotanto")
    print("4) Analysoi kuukausituotanto")
    print("5) Analysoi tuntituotanto")
    print("6) Tallenna kuukausituotanto")
    print("7) Tallenna tuntituotanto")
    print("0) Lopeta")
    valinta = str(input("Valintasi: "))
    #Valikosta palautetaan käyttäjän tekemä valinta
    return valinta


def lue(tiedostonNimi, lista, vuosi):
    #Listan tyhjennys aina ennen sen käsittelyä poistaa ylikirjoitusvirheen.
    lista.clear()
    #Yritetään avata käyttäjän tiedosto
    try:
        with open(tiedostonNimi, "r") as kayttajanTiedosto:
            #Luodaan muuttujat luetuille riveille ja analysoitaville
            luetutRivit = 0
            analysoidutRivit = 0
            #Jatketaan kunnes kaikki rivit on käyty läpi
            for rivi in kayttajanTiedosto:
                #Lasketaan luettu rivi
                luetutRivit += 1
                #Jos rivi ei ala käyttäjän antamalla vuodella, ei tehdä mitään
                if rivi[:4] != vuosi:
                    pass
                else:
                    alkio = rivi.split(";")
                    aika = datetime.datetime.strptime(alkio[0], "%Y-%m-%d %H:%M:%S")
                    rivinTuotanto = float(alkio[1]) + float(alkio[2]) + float(alkio[3]) + float(alkio[4]) + float(alkio[5]) + float(alkio[6]) + float(alkio[7])
                    #Jos rivin tuotanto on negatiivinen, muutetaan se nollaksi
                    if rivinTuotanto < 0:
                        rivinTuotanto = 0
                    #Lisätään listaan aika ja sen tuotanto
                    lista.append([aika, rivinTuotanto])
                    #Lasketaan analysoitu rivi
                    analysoidutRivit += 1
            #Jos lista on tyhjä niin kerrotaan siitä        
            if len(lista) == 0:
                print("Antamanasi vuonna ei löytynyt yhtäkään arvoa.")
            else:
                #Otetaan listasta ensimmäinen ja viimeinen aikaleima
                ekaAikaleima = lista[0][0].strftime("%d.%m.%Y %H:%M")
                vikaAikaleima = lista[-1][0].strftime("%d.%m.%Y %H:%M")
                #Tuolostetaan luetun tiedoston nimi sekä luettujen ja analysoitujen rivien määrä
                print("Tiedosto '{0}' luettu, {1} riviä, {2} otettu analysoitavaksi.".format(tiedostonNimi, luetutRivit, analysoidutRivit))
                #Tulostetaan minkä ajan välistä dataa listassa on
                print("Analysoidaan {0} ja {1} välistä dataa.".format(ekaAikaleima, vikaAikaleima))
                print("")
            return lista
    #Mikäli tiedostoa ei löydy ohjelma lopettaa toiminnan
    except FileNotFoundError:
        print("Tiedoston '{0}' lukeminen epäonnistui, ei löydy, lopetetaan.".format(tiedostonNimi))
        sys.exit(0)


def paivaAnalyysi(listasta, listaan):
    listaan.clear()
    ekaPaiva = listasta[0][0]
    #Luodaan muuttujat paivantuotannolle ja kumulatiiviselle tuotannolle
    paivaTuotanto = float(0)
    kmTuotanto = float(0)
    for i in listasta:
        #Jos rivin päivä on sama kuin ensimmäinen päivä, lasketaan tuotanto (.date() antaa pv,kk ja vuoden)
        #vertailu kuitenkin tapahtuu pienimmän avulla eli päivän
        if i[0].date() == ekaPaiva.date():
            paivaTuotanto += i[1]
        else:
            kmTuotanto += paivaTuotanto #Lasketaan kmTuotantoa
            listaan.append([ekaPaiva, paivaTuotanto, kmTuotanto]) #Lisätään lista listaan
            ekaPaiva += datetime.timedelta(days = 1) #Mennään päivässä eteenpäin
            paivaTuotanto = float(0)
            #Koska tämä elif toiminto suoritettiin rivin i[0] kustannuksella
            #se täytyy lisätä nyt
            paivaTuotanto += i[1]
    kmTuotanto += paivaTuotanto
    #Viimeinen päivä on jo laskettu for loopilla ja tallennettu, mutta se täytyy kirjoittaa manuaalisesti
    listaan.append([ekaPaiva, paivaTuotanto, kmTuotanto])
    print("Päivätuotanto analysoitu.\n")
    return listaan


def paivaTallenna(lista, vuosi):
    #Tehdään tiedoston nimi
    alku = "tulosPaiva"
    vuosi = str(vuosi)
    csv = ".csv"
    tiedostonNimi = (alku + vuosi + csv)
    try:
        #Avataan tiedosto
        with open(tiedostonNimi, "w") as uusiTiedosto:
            #Kirjoitetaan päivittäinen tuotanto tiedostoon
            pvSahko = "Päivittäinen sähköntuotanto:"
            uusiTiedosto.write("{0}\n;{1}\n".format(pvSahko, vuosi))
            for i in lista:
                uusiTiedosto.write("{0};{1}\n".format(i[0].strftime("%d.%m.%Y"), int(i[1])))
            uusiTiedosto.write("\n\n")
            #Kirjoitetaan kumulatiivinen tuotanto tiedostoon
            kmSahko = "Kumulatiivinen päivittäinen sähköntuotanto:"
            uusiTiedosto.write("{0}\n;{1}\n".format(kmSahko, vuosi))
            for i in lista:
                uusiTiedosto.write("{0};{1}\n".format(i[0].strftime("%d.%m.%Y"), int(i[2])))
            uusiTiedosto.write("\n\n")
        print("Päivätuotanto tallennettu tiedostoon '{0}'.\n".format(tiedostonNimi))
    except PermissionError: #Suljetaan ohjelma hallitusti mikäli tiedostoa ei voitu kirjottaa
        print("Tiedostoa ei voitu kirjoittaa.")
        sys.exit(0)


def kuukausiAnalyysi(listasta, listaan):
    #Tässä aliohjelman idea on sama kuin päiväanalyysissä. Nyt vaan vertaillaan kuukausia 
    #ja kumulatiivisen tuotannon sijasta lasketaan tuotanto koko vuodelle
    listaan.clear()
    ekaKuukausi = listasta[0][0]
    kuukausiTuotanto = float(0)
    vuosiTuotanto = float(0)
    for i in listasta:
        if datetime.datetime.strftime(i[0], "%m") == datetime.datetime.strftime(ekaKuukausi, "%m"):
            kuukausiTuotanto += i[1]
        else:
            vuosiTuotanto += kuukausiTuotanto
            listaan.append([ekaKuukausi, kuukausiTuotanto])
            ekaKuukausi = i[0]
            kuukausiTuotanto = float(0)
            kuukausiTuotanto += i[1]
    vuosiTuotanto += kuukausiTuotanto        
    listaan.append([ekaKuukausi, kuukausiTuotanto, vuosiTuotanto]) #Vuosituotanto lisätty vikan listan loppuun
    print("Kuukausituotanto analysoitu.\n")
    return(listaan)


def tuntiAnalyysi(listasta, listaan): 
    vuosiTuotanto = float(0) 
    #Tunti analyysi on toteutettu matriisin avulla. Tällä tavalla olisi voinut
    #suorittaa tehokkaasti myös kaikki muut analyysit.
    for i in listasta:
        vuosiTuotanto += i[1]
        ekaTunti = int(datetime.datetime.strftime(i[0], "%H"))
        ekaKuukausi = int(datetime.datetime.strftime(i[0], "%m")) - 1
        #Eli listaan[paikka][paikka] lisätään arvo i[1]
        #idean hoksaa kun hahmottelee algoritmin toiminnan paperille
        #Listan sisällä on listoja aka kokonaisuus on matriisi
        #se on luotu ennen tätä aliohjelmaa ja täytetty 0
        #Ei se ole sen kummempaa.
        listaan[ekaKuukausi][ekaTunti] += i[1]
    #"Täytetään" loppuun kuukausittaisekka yhteistuotannolla tunneille
    for i in range(12):
        for j in range(24):
            listaan[12][j] += listaan[i][j]
    #Lisätään viimeiseksi listaksi vuosituotanto
    listaan.append([vuosiTuotanto])
    print("Tuntituotanto analysoitu.\n")
    return listaan


def kuukausiTallenna(lista, vuosi):
    alku = "tulosKuukausi"
    vuosi = str(vuosi)
    csv = ".csv"
    tiedostonNimi = (alku + vuosi + csv)
    try:
        with open(tiedostonNimi, "w") as uusiTiedosto:
            #Tehdään muutama muuttuja
            vuosiTuotanto = lista[-1][2]
            kkSahko = "Kuukausittainen sähköntuotanto:"
            prosentti = "%-osuus"
            #Kirjoitetaan rivi kauniisti .formatilla
            uusiTiedosto.write("{0}\n;{1};{2}\n".format(kkSahko, vuosi, prosentti))
            for i in lista:
                osuus = i[1] / vuosiTuotanto * 100 #Vähän fuksimatikkaa. edit/kurssin ka. on 1.41 joten lauseke on aika simppeli
                uusiTiedosto.write(" {0};{1};{2}%\n".format(i[0].strftime("%m/%Y"), int(i[1]), int(osuus)))
            uusiTiedosto.write("Yhteensä;{0}".format(int(vuosiTuotanto)))
            uusiTiedosto.write("\n\n\n")
        print("Kuukausituotanto tallennettu tiedostoon '{0}'.\n".format(tiedostonNimi))
    except PermissionError: #Ilmoitetaan käyttäjälle virheestä ja kaadetaan ohjelma hallitusti
        print("Tiedostoa ei voitu kirjoittaa.")
        sys.exit(0)


def tuntiTallenna(lista, vuosi):
    alku = "tulosTunti"
    vuosi = str(vuosi)
    csv = ".csv"
    tiedostonNimi = (alku + vuosi + csv)
    try:
        with open(tiedostonNimi, "w") as uusiTiedosto:
            vuosiTuotanto = lista[-1][0]
            tuntiSahko = "Tuntipohjainen sähköntuotanto:"
            uusiTiedosto.write("{0}\n".format(tuntiSahko))
            #Kirjoitetaan rivi tunneille
            for i in range(24):
                uusiTiedosto.write(";{0}".format(i))
            uusiTiedosto.write("\n")
            #Tehdään matriisi ja samalla tulostetaan for loopilla oikea kk ja arvo
            for i in range(12):
                uusiTiedosto.write(" {:02d}/{%d}".format(i + 1, vuosi))
                for j in range(24):
                    uusiTiedosto.write(";{0}".format(int(lista[i][j])))
                uusiTiedosto.write("\n")
            #Kirjoitetaan yhteensä rivit
            uusiTiedosto.write("Yhteensä")
            for i in range(24):
                uusiTiedosto.write(";{0}".format(int(lista[-2][i])))
            uusiTiedosto.write("\n\n\n")
            #Yksittäisen tunnin osuus
            uusiTiedosto.write("Yksittäisen tunnin osuus vuosittaisesta sähköntuotannosta:\n")
            for i in range(24):
                uusiTiedosto.write(";{0}".format(i))
            uusiTiedosto.write("\n%-osuus")
            for i in range(24):
                osuus = lista[-2][i] / vuosiTuotanto * 100
                uusiTiedosto.write(";{0}%".format(int(osuus)))
            uusiTiedosto.write("\n\n\n")
        print("Tuntituotanto tallennettu tiedostoon '{0}'.\n".format(tiedostonNimi))
    except PermissionError: #Ilmoitetaan käyttäjälle virheestä ja kaadetaan ohjelma hallitusti
        print("Tiedostoa ei voitu kirjoittaa.")
        sys.exit(0)


def Alexa(): #tälle on ollut käyttöä
    webbrowser.open("https://youtu.be/kJQP7kiw5Fk?t=24")
    print("Playing Despacito by Luis Fonsi.\n")


#19.04.2017 Never forget
